Simple Sass Background Slider

This is a simple Sass mixin that will allow you to do this:

@include bg-slider(myslider, 800px, 500px, 10, horizontal, url(/path-to-image), url(/path-to-image-2), url(/path-to-image-3);

to do this: 

http://codepen.io/WereHare/pen/mIxrG/

Just import the partial and go wild. 

@import "_bg-slider";

Arguments are (all required):

1. name of animation and generated class,
2. width,
3. height,
4. slide duration,
5. direction (horizontal or vertical),
6,7... any number of background-image arguments


Generated class may be styled normally outside of the include. e.g.:

 .myslider {
		//additional styles here will apply to generated slider class.
	}

Notes: 

Background-size is set to cover on all supplied arguments.

It is possible to reset the sizing of the element to auto or percentage, just bear in mind that the sizes you supply to the mixin are used to calculate the sliding speed.
